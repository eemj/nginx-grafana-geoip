module gitlab.com/eemj/nginx-geoip

go 1.14

require (
	github.com/fsnotify/fsnotify v1.4.9
	github.com/hpcloud/tail v1.0.0
	github.com/influxdata/influxdb v1.8.1
	github.com/influxdata/influxdb-client-go v1.4.0
	github.com/influxdata/influxdb1-client v0.0.0-20200515024757-02f0bf5dbca3
	github.com/mmcloughlin/geohash v0.10.0
	github.com/oschwald/geoip2-golang v1.4.0
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
