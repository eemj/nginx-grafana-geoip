package main

import (
	"flag"
	"log"
	"net"
	"net/url"
	"os"
	"os/signal"
	"strings"

	"github.com/hpcloud/tail"
	"github.com/influxdata/influxdb1-client"
	"github.com/mmcloughlin/geohash"
	"github.com/oschwald/geoip2-golang"
)

func main() {
	var username,
		password,
		database,
		host,
		measurement,
		geoipPath,
		accessLog string

	flag.StringVar(&username, "u", "", "influxdb username")
	flag.StringVar(&password, "p", "", "influxdb password")
	flag.StringVar(&database, "c", "", "influxdb database name")
	flag.StringVar(&measurement, "m", "", "influxdb measurement")
	flag.StringVar(&host, "addr", "", "influxdb host (e.g: http://127.0.0.1:8086..)")
	flag.StringVar(&geoipPath, "geoip", "", "geoip2 path")
	flag.StringVar(&accessLog, "f", "", "nginx access log")

	flag.Parse()

	db, err := geoip2.Open(geoipPath)

	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	t, err := tail.TailFile(accessLog, tail.Config{
		Follow: true, MustExist: true,
	})

	if err != nil {
		log.Fatal(err)
	}

	url, err := url.Parse(host)

	if err != nil {
		return
	}

	c, err := client.NewClient(client.Config{
		Username: username,
		Password: password,
		Timeout:  client.DefaultTimeout,
		URL:      *url,
	})

	if err != nil {
		log.Fatal(err)
	}

	s := make(chan os.Signal, 1)
	signal.Notify(s, os.Interrupt)

	for {
		select {
		case <-s:
			return
		case line := <-t.Lines:
			space := strings.Index(line.Text, " ")
			address := net.ParseIP(line.Text[:space])
			country, _ := db.Country(address)
			city, _ := db.City(address)

			point := client.Point{
				Fields: map[string]interface{}{"count": 1},
				Tags: map[string]string{
					"host":         address.String(),
					"country_code": country.Country.IsoCode,
					"geohash": geohash.Encode(
						city.Location.Latitude, city.Location.Longitude,
					),
				},
				Measurement: measurement,
			}

			_, err := c.Write(client.BatchPoints{
				Points:   []client.Point{point},
				Database: database,
			})

			if err != nil {
				log.Fatal(err)
			}
		}
	}
}

func parse() {
}
